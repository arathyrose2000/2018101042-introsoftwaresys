from flask import Flask, request, flash, url_for, redirect, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URL'] = 'sqlite:///tmp/test.db'
db = SQLAlchemy(app)


class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String, unique=True)
    Roll = db.Column(db.Integer, unique=True)
    Email = db.Column(db.String, unique=True)

    def __init__(self, name, roll, email):
        self.Name = name
        self.Roll = roll
        self.Email = email


@app.route('/students/create', methods=["POST", "GET"])
def addStd():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        roll = request.form['rollnumber']
        db.create_all()
        new_std = Student(name, roll, email)
        db.session.add(new_std)
        db.session.commit()
        temp = {}
        temp['status'] = (type(new_std) == Student)
        temp['name'] = (new_std.Name)
        temp['roll'] = (new_std.Roll)
        temp['email'] = (new_std.Email)
        return jsonify(temp)
    else:
        return render_template('new_stu.html')


@app.route('/students', methods=["GET"])
def showStds():
    db.create_all()
    studs = Student.query.all()
    studsstr = {}
    for j in studs:
        studsstr[j.id] = {"name": j.Name, "roll": j.Roll, "email": j.Email}
    return jsonify(studsstr)


@app.route('/students/delete', methods=["POST", "GET"])
def delStds():
    if request.method == 'POST':
        roll = request.form['rollnumber']
        db.create_all()
        stud = Student.query.filter_by(Roll=roll).first()
        if(stud):
            db.session.delete(stud)
            db.session.commit()
            temp = {}
            temp['status'] = "Deleted!  " + stud.Name
            return jsonify(temp)
        else:
            temp = {}
            temp['status'] = "Not found!  "
            return jsonify(temp)
    else:
        return render_template('del_stu.html')


@app.route('/students/<rollnumber>', methods=["POST", "GET"])
def updtStds(rollnumber):
    if request.method == 'POST':
        roll = rollnumber
        name = request.form['name']
        email = request.form['email']
        db.create_all()
        stud = Student.query.filter_by(Roll=roll).first()
        if(stud):
            stud.Name = name
            stud.Email = email
            db.session.commit()
            return jsonify(stud)
        else:
            temp = {}
            temp['status'] = "Not found"
            return jsonify(temp)
    else:
        db.create_all()
        stud = Student.query.filter_by(Roll=rollnumber).first()
        temp = {}
        if(stud):
            temp['name'] = stud.Name
            temp['rollnumber'] = stud.Roll
            temp['email'] = stud.Email
            return jsonify(temp)
        else:
            temp['status'] = "Not found"
            return jsonify(temp)


class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String, unique=True)
    Code = db.Column(db.Integer, unique=True)

    def __init__(self, name, code):
        self.Name = name
        self.Code = code


@app.route('/courses/create', methods=["POST", "GET"])
def addCrs():
    if request.method == 'POST':
        name = request.form['name']
        code = request.form['code']
        db.create_all()
        new_crs = Course(name, code)
        db.session.add(new_crs)
        db.session.commit()
        temp = {}
        temp['status'] = (type(new_crs) == Course)
        temp['name'] = (new_crs.Name)
        temp['code'] = (new_crs.Code)
        return jsonify(temp)
    else:
        return render_template('new_course.html')


@app.route('/courses', methods=["GET"])
def showCrs():
    db.create_all()
    studs = Course.query.all()
    studsstr = {}
    for j in studs:
        studsstr[j.id] = {"name": j.Name, "code": j.Code}
    return jsonify(studsstr)


@app.route('/courses/<code>/delete', methods=["POST"])
def delCrs(code):
    db.create_all()
    stud = Course.query.filter_by(Code=code).first()
    if(stud):
        db.session.delete(stud)
        db.session.commit()
        temp = {}
        temp['status'] = "Deleted!" + stud.Name
        return jsonify(temp)
    else:
        temp = {}
        temp['status'] = "Not found"
        return jsonify(temp)


@app.route('/courses/<code>', methods=["POST", "GET"])
def updtCrs(code):
    if request.method == 'POST':
        name = request.form['name']
        code = request.form['code']
        db.create_all()
        stud = Course.query.filter_by(Code=code).first()
        if(stud):
            stud.Name = name
            stud.Code = code
            db.session.commit()
            temp = {}
            temp['status'] = stud.Name == name and stud.Code == code
            return jsonify(temp)
        else:
            temp = {}
            temp['status'] = "Not found"
            return jsonify(temp)

    else:
        db.create_all()
        stud = Course.query.filter_by(Code=code).first()
        temp = {}
        if(stud):
            temp['name'] = stud.Name
            temp['code'] = stud.Code
            return jsonify(temp)
        else:
            temp['status'] = "Not found"
            return jsonify(temp)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5000)

