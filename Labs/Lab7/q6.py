import requests
r = requests.get(
    "https://api.opendota.com/api/players/431931403/heroes?significant=0")
a = r.json()
for i in a:
    if i['hero_id'] == '74':
        try:
            print(int(i['win'])/int(i['games']))
        except:
            print("No games played")
max_win = 0
for i in a:
    if int(i['games']) >= 6:
        try:
            win = ((i['win'])/int(i['games'])) * 100
        except:
            win = 0
        if win > max_win:
            max_win = win
            b = i
print(b['hero_id'])
