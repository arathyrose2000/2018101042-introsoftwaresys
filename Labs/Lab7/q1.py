def isLeapYear(y):
    if(y % 100 == 0):
        if(y % 400 == 0):
            return True
        else:
            return False
    elif(y % 4 == 0):
        return True
    else:
        return False


year = int(input("Enter the year: "))
print(isLeapYear(year))
