import sys
program_name = sys.argv[0]
arguments = sys.argv[1:]
count = len(arguments)
if count != 2:
    exit()
inp = arguments[0]
outp = arguments[1]

counts = {}
punctuations = r'''!()-[]{};:'"\,<>./?@#$%^&*_~'''
line_nopunct = " "

ins = open(inp)
str = ins.read()
ins.close()

f = open(inp, 'r')
ans = f.read()
ans = ans.replace(".", " ")
ans = ans.replace(",", " ")
ans = ans.replace(":", " ")
ans = ans.replace(";", " ")
ans = ans.replace("!", " ")
ans = ans.replace("?", " ")
ans = ans.replace(")", " ")
ans = ans.replace("(", " ")
ans = ans.replace("\"", " ")
ans = ans.replace("\n", " ")
ans = ans.split(" ")
try:
    ans = [x for x in ans if x != ""]
except:
    ans = ans
counts = {}
for word in ans:
    if word.upper() in (count.upper() for count in counts):
        try:
            counts[word] += 1
        except:
            for i in counts:
                if i.upper() == word.upper():
                    counts[i] += 1
    else:
        counts[word] = 1
f.close()
with open(outp, 'w') as f:
    for x in counts:
        print(x, ":", counts[x], file=f)
