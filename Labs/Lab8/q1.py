class matrix:
    def __init__(self, n, m, *args):
        if(args):
            mat = args[0]
        self.a = []
        self.n = n
        self.m = m
        for i in range(self.n):
            temp = []
            for j in range(self.m):
                if(args):
                    temp.append(mat[i][j])
                else:
                    temp.append(0)
            self.a.append(temp)

    def display(self):
        for i in range(self.n):
            for j in range(self.m):
                print(self.a[i][j], end=' ')
            print()

    def transpose(self):
        t = []
        for j in range(self.m):
            temp = []
            for i in range(self.n):
                temp.append(self.a[i][j])
            t.append(temp)
        trans = matrix(self.n, self.m, t)
        return trans

    def __getitem__(self, i):
        return self.a[i]

    def __setitem__(self, i, a):
        self.a[i] = a

    def __str__(self):
        self.display()
        return ""

    def __add__(self, a):
        if(self.n == a.n and self.m == a.m):
            t = []
            for j in range(self.m):
                temp = []
                for i in range(self.n):
                    temp.append(self.a[i][j]+a.a[i][j])
                t.append(temp)
            return matrix(self.n, self.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __sub__(self, a):
        if(self.n == a.n and self.m == a.m):
            t = []
            for j in range(self.m):
                temp = []
                for i in range(self.n):
                    temp.append(self.a[i][j]-a.a[i][j])
                t.append(temp)
            return matrix(self.n, self.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __mul__(self, a):
        if(self.m == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(a.m):
                    l = 0
                    for k in range(a.n):
                        l += self.a[i][k] * a.a[k][j]
                    temp.append(l)
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __pow__(self, a):
        t = []
        for i in range(self.n):
            temp = []
            for j in range(self.m):
                temp.append(self.a[i][j]*a)
            t.append(temp)
        return matrix(self.n, self.m, t)

    def __floordiv__(self, a):
        t = []
        for i in range(self.n):
            temp = []
            for j in range(self.m):
                temp.append(self.a[i][j]/a)
            t.append(temp)
        return matrix(self.n, self.m, t)

    def __eq__(self, a):
        if(self.m == a.m and self.n == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(self.m):
                    temp.append(self.a[i][j] == a.a[i][j])
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __ne__(self, a):
        if(self.m == a.m and self.n == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(self.m):
                    temp.append(self.a[i][j] != a.a[i][j])
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __lt__(self, a):
        if(self.m == a.m and self.n == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(self.m):
                    temp.append(self.a[i][j] < a.a[i][j])
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __le__(self, a):
        if(self.m == a.m and self.n == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(self.m):
                    temp.append(self.a[i][j] <= a.a[i][j])
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __gt__(self, a):
        if(self.m == a.m and self.n == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(self.m):
                    temp.append(self.a[i][j] > a.a[i][j])
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self

    def __ge__(self, a):
        if(self.m == a.m and self.n == a.n):
            t = []
            for i in range(self.n):
                temp = []
                for j in range(self.m):
                    temp.append(self.a[i][j] >= a.a[i][j])
                t.append(temp)
            return matrix(self.n, a.m, t)
        else:
            print("OPERATION INVALID")
            return self


class vector(matrix):
    def __init__(self, n, *args):
        self.n = n
        self.m = 1
        self.a = []
        if(args):
            mat = args[0]
        for i in range(self.n):
            temp = []
            if(args):
                temp.append(mat[i])
            else:
                temp.append(0)
            self.a.append(temp)

    def __getitem__(self, i):
        return self.a[i][0]

    def __setitem__(self, i, a):
        self.a[i][0] = a

    def norm(self):
        sum = 0
        for i in range(self.n):
            sum += (self.a[i][0]*self.a[i][0])
        sum = sum**0.5
        return sum

    def __mod__(self, a):
        if(self.n == a.n):
            temp = 0
            for i in range(self.n):
                temp+=(self.a[i][0]*a.a[i][0])
            return temp
        else:
            print("Invalid operation")

    def transpose(self):
        print("Invalid operation")


l = matrix(3, 3)
# print(l)
k = [[1, 9, 0], [0, 1, 0], [0, 0, 1]]
b = matrix(3, 3, k)
a = matrix(3, 3, k)

# b[0][1] = 100
print(b[0][1])
print()
trans = b.transpose()
print(b)
print(trans+b)
print(trans-b)

print(trans*b)
print(trans**1000)
print(trans//1000)

print(trans == b)
print(a == b)

arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
vect = vector(10, arr)
print(vect)

print(vect.norm())

print("\n")

print(vect%vect)
