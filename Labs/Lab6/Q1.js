$(document).ready(function() {
    $('p').mousedown(function(event) {
        switch (event.which) {
            case 1:
                $(this).hide();
                break;
            case 3:
                $(this).html("<b>" + $(this).text() + "</b>");
                break;
        }
    })
    $("#resetall").click(function() {
        location.reload(true);
    });
    $("#someimage").fadeOut(1000);
});