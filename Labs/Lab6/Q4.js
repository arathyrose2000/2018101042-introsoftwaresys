function validateForm() {
    $.ajax({
        url: 'http://localhost:8000/getmarks',
        method: 'POST',
        datatype: 'json',
        data: {
            'letter': document.forms["LETT"]["letter"].value,
        },
        success: function(response) {
            $("#td").empty();
            var student = response;
            console.log(student);
            for (var i in student) {
                var per = student[i][0] + student[i][1] + student[i][2] + student[i][3] + student[i][4];
                per = per / 5;
                var grade;
                if (per > 90) grade = 'A'
                else if (per > 80) grade = 'B'
                else if (per > 70) grade = 'C'
                else if (per > 60) grade = 'D'
                else grade = 'F'
                $("#td").append($('<tr><td>' + i + '</td><td>' + student[i][0] + '</td><td>' + student[i][1] + '</td><td>' + student[i][2] + '</td><td>' + student[i][3] + '</td><td>' + student[i][4] + '</td><td>' + per + '</td><td>' + grade + '</td></tr>'));
            }
        },
        error: function(response) {
            alert("Can't load");
        }
    })
    return false;
}