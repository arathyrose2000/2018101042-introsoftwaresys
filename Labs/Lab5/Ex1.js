function fifthmonth(s) {
	months = {
		'January': 'June',
		'February': 'July',
		'March': 'August',
		'April': 'September',
		'May': 'October',
		'June': 'November',
		'July': 'December',
		'August': 'January',
		'September': 'February',
		'October': 'March',
		'November': 'April',
		'December': 'May'
	}
	return months[s];
}
//Displays the result to the console log
console.log(fifthmonth('May'))

