var data = require('./data.js')
function getHighestMarks() {
	var maxstu
	var maxtotal = 0
	for (name in data) {
		var total = 0;
		for (var i = 0; i < 5; i++)
			total += data[name][i]
		if (total > maxtotal) {
			maxtotal = total
			maxstu = name
		}
	}
	return maxstu
}
console.log(getHighestMarks())

function getSubject2Toppers() {
	var array = []
	for (name in data)
		array.push([name, data[name][1]])
	array.sort(function (a, b) { return b[1] - a[1] });
	var sortdata = []
	for (i in array)
		sortdata.push(array[i][0])
	return sortdata
}
console.log(getSubject2Toppers())
