function myArray(s) {
	array = s
	this.Sum = function () {
		var sum = 0
		for (var i = 0; i < array.length; i++)
			sum += array[i];
		return sum;
	}
	this.Product = function () {
		var pro = 1
		for (var i = 0; i < array.length; i++)
			pro *= array[i];
		return pro;
	}
	this.Sort = function () {
		for (var i = 0; i < array.length; i++)
			for (var j = 0; j < array.length - i - 1; j++)
				if (array[j] > array[j + 1]) {
					var p = array[j]
					array[j] = array[j + 1]
					array[j + 1] = p
				}
	}
	this.Modify = function (i, value) {
		array[i] = value
	}
	this.Display = function () {
		console.log(array);
	}
}
var S = new myArray([1, 2, 3, 6, 4, 5, 9, 7, 8, 0])
S.Display()
console.log(S.Sum())
console.log(S.Product())
console.log(S.Sort())
S.Display()
console.log(S.Modify(1, 100))
S.Display()

