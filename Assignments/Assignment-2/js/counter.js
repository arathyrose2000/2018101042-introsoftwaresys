if (typeof(Storage) !== "undefined") {
    if (localStorage.clickcount) {
        localStorage.clickcount = parseInt(localStorage.clickcount);
    } else {
        localStorage.clickcount = 0;
    }
    document.getElementById("like_counter").innerHTML = "Current number of likes:  " + localStorage.clickcount; + ".";
}

function increment_counter() {
    if (typeof(Storage) !== "undefined") {
        if (localStorage.clickcount) {
            localStorage.clickcount = parseInt(localStorage.clickcount) + 1;
        } else {
            localStorage.clickcount = 1;
        }
        document.getElementById("like_counter").innerHTML = "Current number of likes:  " + localStorage.clickcount; + ".";
    }
}