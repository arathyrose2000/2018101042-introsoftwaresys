function CP() {
    document.getElementById('CP').className = "items active_1"
    document.getElementById('reading').className = "items non_active"
    document.getElementById('wd').className = "items non_active"
    document.getElementById('other').className = "items non_active"
    document.getElementById('default').style.display = "none"
    document.getElementById('Computer Programming').style.display = "block"
    document.getElementById('Web-designing').style.display = "none"
    document.getElementById('Reading').style.display = "none"
    document.getElementById('Other Interests').style.display = "none"
}

function reading() {
    document.getElementById('CP').className = "items non_active"
    document.getElementById('reading').className = "items active_1"
    document.getElementById('wd').className = "items non_active"
    document.getElementById('other').className = "items non_active"
    document.getElementById('default').style.display = "none"
    document.getElementById('Computer Programming').style.display = "none"
    document.getElementById('Web-designing').style.display = "none"
    document.getElementById('Reading').style.display = "block"
    document.getElementById('Other Interests').style.display = "none"
}

function wd() {
    document.getElementById('CP').className = "items non_active"
    document.getElementById('reading').className = "items non_active"
    document.getElementById('wd').className = "items active_1"
    document.getElementById('other').className = "items non_active"
    document.getElementById('default').style.display = "none"
    document.getElementById('Computer Programming').style.display = "none"
    document.getElementById('Web-designing').style.display = "block"
    document.getElementById('Reading').style.display = "none"
    document.getElementById('Other Interests').style.display = "none"
}

function other() {
    document.getElementById('CP').className = "items non_active"
    document.getElementById('reading').className = "items non_active"
    document.getElementById('wd').className = "items non_active"
    document.getElementById('other').className = "items active_1"
    document.getElementById('default').style.display = "none"
    document.getElementById('Computer Programming').style.display = "none"
    document.getElementById('Web-designing').style.display = "none"
    document.getElementById('Reading').style.display = "none"
    document.getElementById('Other Interests').style.display = "block"
}