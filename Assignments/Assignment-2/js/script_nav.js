/*For the navigation bar*/
/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function responsive_topnav_fun() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else if (x.className === "topnav sticky") {
        x.className = "topnav responsive sticky";
    } else if (x.className === "topnav responsive sticky") {
        x.className = "topnav sticky";
    } else {
        x.className = "topnav";
    }
}
/*STICKY*/
// When the user scrolls the page, execute scroll_fun
window.onscroll = function() {
    scroll_fun()
}

// Get the myTopnav
var myTopnav = document.getElementById("myTopnav");

// Get the offset position of the myTopnav
var sticky = myTopnav.offsetTop;

// Add the sticky class to the myTopnav when you reach its scroll position. Remove "sticky" when you leave the scroll position
function scroll_fun() {
    if (myTopnav.className === "topnav responsive") {
        myTopnav.className = "topnav sticky";
    } else if (myTopnav.className === "topnav responsive sticky") {
        myTopnav.className = "topnav sticky";
    } else if (window.pageYOffset >= sticky) {
        myTopnav.classList.add("sticky")
    } else {
        myTopnav.classList.remove("sticky");
    }
}